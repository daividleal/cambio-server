<?php
require_once './../lib/config.php';
$response = Array();

$id = posted("id", PTRN_NUMBER);
$like = posted("like");
$cambistaId = posted("cambistaId", PTRN_NUMBER);

$sql = "select * 
        from :table 
        where (cambistaId1=".$id." and cambistaId2=".$cambistaId.") 
			   or (cambistaId1=".$cambistaId." and cambistaId2=".$id.")";

$match = CambistaMatch::sql($sql, SimpleOrm::FETCH_ONE);

try {
	if($match){	
		if($match->cambistaId1 == $id){
			$match->cambistaMatch1 = $like;
		}else{
			$match->cambistaMatch2 = $like;
		}
		$match->segundaAvaliacao = 1;
		$match->save();
		$match = CambistaMatch::sql($sql, SimpleOrm::FETCH_ONE);
		$response['cambistaMatch'] = $match->getLoadedData();
	}else{
		$cambistaMatch = new CambistaMatch();
		$cambistaMatch->cambistaId1  = $id;
		$cambistaMatch->cambistaMatch1  = $like;
		$cambistaMatch->cambistaId2  = $cambistaId;
		$cambistaMatch->cambistaMatch2 = 0; //Valor default para n�o levantar exception
		$cambistaMatch->primeiraAvaliacao = 1;
		$cambistaMatch->segundaAvaliacao = 0;
		$cambistaMatch->save();
		$match = CambistaMatch::sql($sql, SimpleOrm::FETCH_ONE);
		$response['cambistaMatch'] = $match->getLoadedData();
	}
	$response['success'] = true;	
} catch (Exception $e) {
	$response['cambistaMatch'] = null;
	$response['success'] = false;
	//echo($e->getMessage());
}

echo(json_encode($response, JSON_UNESCAPED_UNICODE));
?>