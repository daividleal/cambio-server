<?php
require_once './../lib/config.php';

$response = Array();
$id = posted("id", PTRN_NUMBER);
$page = posted("page", PTRN_NUMBER);

$items_per_page = 500;
$offset = $page * $items_per_page;
$sql = "SELECT * 
        FROM :table
        WHERE cambista.id in
            (select cambistaId1 as id 
             from cambista_match 
             where cambistaId2=".$id." and cambistaMatch1 = 1 and cambistaMatch2 = 1 
             union 
             select cambistaId2 as id 
             from cambista_match 
             where cambistaId1=".$id." and cambistaMatch1 = 1 and cambistaMatch2 = 1) limit ".$offset.", ".$items_per_page;

try {
    $cambistas = Cambista::sql($sql);
    $data = Array();
    foreach($cambistas as $c){
        array_push($data, $c->getLoadedData());
    }
    array_walk_recursive($data, function(&$value, $key) {
    	if (is_string($value)) {
    		$value = iconv('windows-1252', 'utf-8', $value);
    	}
    });
    $response['cambistas'] = $data;
    $response['success'] = true;
} catch (Exception $e) {
    $response['success'] = false;
    //echo($e->getMessage());
}

echo(json_encode($response, JSON_UNESCAPED_UNICODE));
?>