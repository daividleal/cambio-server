<?php
require_once './../lib/config.php';
$response = Array();

$email = posted("email", PTRN_NUMBER);

try {
	$cambista = Cambista::retrieveByEmail(posted('email'), SimpleOrm::FETCH_ONE);
	if($cambista){
		$response['registered'] = true;
	}else{
		$response['registered'] = false;
	}
} catch (Exception $e) {
	//echo($e->getMessage());
}

echo(json_encode($response, JSON_UNESCAPED_UNICODE));
?>