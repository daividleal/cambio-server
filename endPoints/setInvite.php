<?php
require_once './../lib/config.php';
$response = Array();

$id = posted("id", PTRN_NUMBER);
$firstName = posted("firstName", PTRN_ALL_NOBLANK);
$invite = posted("invite");

try {
	$invitation = getInvitation($id);
	if($invitation){
		$response['invited'] = true;
	}else{
		$response['invited'] = false;

				
		$idInvitation = explode("-", $invite);
		if(count($idInvitation) > 1){
			$invitation = getInvitation($idInvitation[1]);	
		}else{
			$invitation = null;
		}
		
		
		if($invitation){	
			$invitation->qtd = $invitation->qtd + 1;
			$invitation->save();
			$response['earn'] = true;
			$invitation = new Invitation();
			$invitation->idInvitation = $id;
			$invitation->invite = $firstName."-".$id;
			$invitation->qtd = 10;
			$invitation->save();
		}else{
			$invitation = new Invitation();
			$response['earn'] = false;
			$invitation->idInvitation = $id;
			$invitation->invite = $firstName."-".$id;
			$invitation->qtd = 0;
			$invitation->save();
		}
	}
	
	$response['success'] = true;
} catch (Exception $e) {
	$response['success'] = false;
	$response['invited'] = false;
	//echo($e->getMessage());
}

echo(json_encode($response, JSON_UNESCAPED_UNICODE));
?>
