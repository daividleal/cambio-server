<?php
require_once './../lib/config.php';

$response = Array();
$id = posted("id", PTRN_NUMBER);

try {
	$cambista = Cambista::retrieveByPK($id, SimpleOrm::FETCH_ONE);	
	if($cambista){
		$cambista->latitude = posted('latitude'); //double
		$cambista->longitude = posted('longitude'); //double
		$cambista->interesse = posted('interesse');
		$cambista->moeda = posted('moeda');
		$cambista->escolaridade = posted('escolaridade');
		$cambista->save();
		$response['success'] = true;
	}else{
		$response['success'] = false;
	}
} catch (Exception $e) {
    $response['success'] = false;
    //echo($e->getMessage());
}

echo(json_encode($response, JSON_UNESCAPED_UNICODE));
?>