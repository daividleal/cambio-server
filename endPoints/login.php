<?php
require_once './../lib/config.php';
$email = posted('email', PTRN_ALL_NOBLANK);
$senha = posted('senha');
$socialNetworkId = posted('socialNetworkId');
$isSocialNetwork = posted('isSocialNetwork'); 
$response = Array();

try {
	$cambista = null;
	if($isSocialNetwork){
		$cambista = Cambista::sql("select * from cambista where socialNetworkId=".$socialNetworkId, SimpleOrm::FETCH_ONE);
	}else{
		$cambista = Cambista::sql("select * from cambista where email='".$email."' and socialNetworkId=''", SimpleOrm::FETCH_ONE);
	}
	
	if($cambista != null){
		$data = $cambista->getLoadedData();
		array_walk_recursive($data, function(&$value, $key) {
			if (is_string($value)) {
				$value = iconv('windows-1252', 'utf-8', $value);
			}
		});

		if($isSocialNetwork){
			if($data['socialNetworkId'] == $socialNetworkId){
				$response['cambista'] = $data;
			}else{
				$response['cambista'] = null;
			}
		}else{
			if($data['senha'] == $senha){
				$response['cambista'] = $data;
			}else{
				$response['cambista'] = null;
			}
		}
		
	}else{
		$response['cambista'] = null;
	}
	$response['success'] = true;
} catch (Exception $e) {
	$response['success'] = false;
	echo($e->getMessage());
}

echo(json_encode($response, JSON_UNESCAPED_UNICODE));
?>