<?php
require_once './../lib/config.php';

$response = Array();

$newCambista = new Cambista();
//id � auto incremet
$newCambista->socialNetworkId = posted('socialNetworkId', PTRN_ALL_NOBLANK); 
//not null, se n�o houver deixar em branco para cadastro nativos
$newCambista->nome = posted('nome', PTRN_ALL_NOBLANK); //not null
$newCambista->profileUrl = posted('profileUrl');
$newCambista->imageUrl = posted('imageUrl');
$newCambista->email = posted('email', PTRN_ALL_NOBLANK); //not null
$newCambista->senha = posted('senha');
$newCambista->birthday = posted('birthday'); // varchar(10) formato dd-MM-aaaa
$newCambista->active = true; // boolean not null
$newCambista->latitude = posted('latitude'); //double
$newCambista->longitude = posted('longitude'); //double
$newCambista->interesse = posted('interesse');
$newCambista->moeda = posted('moeda');
$newCambista->escolaridade = posted('escolaridade');
$newCambista->token = posted('token');

function dados(){
	$cambista = null;
	if(posted('socialNetworkId', PTRN_ALL_NOBLANK) != ""){
		$sql = "select * 
        		from :table 
        		where socialNetworkId='".posted('socialNetworkId', PTRN_ALL_NOBLANK)."' and  email='".posted('email', PTRN_ALL_NOBLANK)."'";
        		
		$cambista = Cambista::sql($sql, SimpleOrm::FETCH_ONE);
	}else{
		$cambista = Cambista::retrieveByEmail(posted('email'), SimpleOrm::FETCH_ONE);
	}

	return $cambista;
}

try {
	$cambista = dados();
	if($cambista != null){
		$cambista->profileUrl = posted('profileUrl');
		$cambista->imageUrl = posted('imageUrl');
		$cambista->birthday = posted('birthday');//varchar(10)formatodd-MM-aaaa
		$cambista->active = true;//booleannotnull
		$cambista->latitude = posted('latitude');//double
		$cambista->longitude = posted('longitude');//double
		$cambista->interesse = posted('interesse');
		$cambista->moeda = posted('moeda');
		$cambista->escolaridade = posted('escolaridade');
		$cambista->token = posted('token');

		$cambista->save();

		$cambista = dados();
		$data = $cambista->getLoadedData();
		array_walk_recursive($data, function(&$value, $key) {
			if (is_string($value)) {
				$value = iconv('windows-1252', 'utf-8', $value);
			}
		});
		$response['cambista'] = $data;
	}else{
		$newCambista->save();
		$data = Cambista::retrieveByEmail(posted('email'), SimpleOrm::FETCH_ONE);
		array_walk_recursive($data, function(&$value, $key) {
			if (is_string($value)) {
				$value = iconv('windows-1252', 'utf-8', $value);
			}
		});
		$response['cambista'] = $data;
	}
	
	$response['success'] = true;
} catch (Exception $e) {
	$response['success'] = false;
	/*echo($e->getMessage());*/
}

echo(json_encode($response, JSON_UNESCAPED_UNICODE));
?>