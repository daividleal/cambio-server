<?php
require_once './../lib/config.php';
$response = Array();

$id = posted("id", PTRN_NUMBER);

try {
    $invitation = getInvitation($id);
    if($invitation){
        $response['invitation'] = $invitation->getLoadedData();
    }else{
        $response['invitation'] = null;
    }
    $response['success'] = true;    
} catch (Exception $e) {
    $response['invitation'] = null;
    $response['success'] = false;  
}

echo(json_encode($response, JSON_UNESCAPED_UNICODE));
?>