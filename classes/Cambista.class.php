<?php

/**
 * @author daividvasconcelos
 *
 * Quando for usar esta classe para salvar o cambista
 * tem que setar os parametros da seguinte forma:
 * 
 * $cambista = new Cambista();
 * //id � auto incremet
 * $cambista->social_network_id = ''; //not null, se n�o houver deixar em branco para cadastro nativos
 * $cambista->nome = ''; //not null
 * $cambista->profileUrl = '';
 * $cambista->imageUrl = '' ;
 * $cambista->email = ''; //not null
 * $cambista->senha = ''; 
 * $cambista->birthday = ''; // varchar(10) formato dd-MM-aaaa
 * $cambista->active = true; // boolean not null
 * $cambista->latitude = ''; //double
 * $cambista->longitude = ''; //double
 * $cambista->interesse = ''; 
 * $cambista->moeda = '';
 * $cambista->escolaridade = '';
 * 
 */
class Cambista extends SimpleOrm{
	protected static $database = 'cambio',
					 $table = 'cambista';
	
}

?>