-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 06-Set-2017 às 00:26
-- Versão do servidor: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cambio`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cambista`
--

CREATE TABLE `cambista` (
  `id` int(11) NOT NULL,
  `socialNetworkId` varchar(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `token` varchar(500) NOT NULL,
  `nome` varchar(200) NOT NULL,
  `profileUrl` varchar(200) DEFAULT NULL,
  `imageUrl` varchar(200) DEFAULT NULL,
  `senha` varchar(200) DEFAULT NULL,
  `birthday` varchar(10) DEFAULT NULL,
  `active` tinyint(1) NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `interesse` text,
  `moeda` text,
  `escolaridade` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cambista_match`
--

CREATE TABLE `cambista_match` (
  `id` int(11) NOT NULL,
  `cambistaId1` int(11) NOT NULL,
  `cambistaMatch1` tinyint(1) DEFAULT NULL,
  `cambistaId2` int(11) NOT NULL,
  `cambistaMatch2` tinyint(1) DEFAULT NULL,
  `primeiraAvaliacao` tinyint(1) DEFAULT NULL,
  `segundaAvaliacao` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estrutura da tabela `invitation`
--

CREATE TABLE `invitation` (
  `id` int(11) NOT NULL,
  `idInvitation` int(11) NOT NULL,
  `invite` text NOT NULL,
  `qtd` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cambista`
--
ALTER TABLE `cambista`
  ADD PRIMARY KEY (`id`,`socialNetworkId`,`email`);

--
-- Indexes for table `cambista_match`
--
ALTER TABLE `cambista_match`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cambistaId1` (`cambistaId1`) USING BTREE,
  ADD KEY `cambistaId2` (`cambistaId2`) USING BTREE;

--
-- Indexes for table `invitation`
--
ALTER TABLE `invitation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idInvitation` (`idInvitation`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cambista`
--
ALTER TABLE `cambista`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
--
-- AUTO_INCREMENT for table `cambista_match`
--
ALTER TABLE `cambista_match`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
--
-- AUTO_INCREMENT for table `invitation`
--
ALTER TABLE `invitation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=0;
--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `cambista_match`
--
ALTER TABLE `cambista_match`
  ADD CONSTRAINT `cambista_match_ibfk_1` FOREIGN KEY (`cambistaId1`) REFERENCES `cambista` (`id`),
  ADD CONSTRAINT `cambista_match_ibfk_2` FOREIGN KEY (`cambistaId2`) REFERENCES `cambista` (`id`);

--
-- Limitadores para a tabela `invitation`
--
ALTER TABLE `invitation`
  ADD CONSTRAINT `invitation_ibfk_2` FOREIGN KEY (`idInvitation`) REFERENCES `cambista` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
