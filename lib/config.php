<?php
require_once 'SimpleOrm.class.php';
// Load parameters.
$params = parse_ini_file(sprintf('./../lib/parameters.ini', __DIR__), true);
// Connect to the database using mysqli
$conn = new mysqli($params['database']['host'], $params['database']['user'], $params['database']['password']);

if ($conn->connect_error) die(sprintf('Unable to connect to the database. %s', $conn->connect_error));

// Tell SimpleOrm to use the connection you just created.
SimpleOrm::useConnection($conn, $params['database']['name']);

require_once './../classes/Cambista.class.php';
require_once './../classes/CambistaMatch.class.php';
require_once './../classes/Invitation.class.php';
require_once './../lib/functions.php';
?>