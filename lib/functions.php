<?php
// Paterns
/******************************************************************/
define('PTRN_ALL', '/.*/');
define('PTRN_ALL_NOBLANK', '/.+/');
define('PTRN_ALL_NOBLANK_TRIM', '/[^\s]+/');
define('PTRN_DATE', '/^(0?[0-9]|[12][0-9]|3[01])\/(0?[1-9]|1[0-2])\/(19[7-9][0-9]|20[0-2][0-9])$/');
define('PTRN_TIME', '/^(0?[0-9]|1[0-9]|2[0-3])\:([0-5][0-9])(\:(0?[0-9]|[1-5][0-9]))?$/');
define('PTRN_01', '/^[01]$/');
define('PTRN_BRANCH', '/^[123]$/');
define('PTRN_NUMBER', '/^[0-9.]+$/');
define('PTRN_ON', '/^on$/');

function encodeArray($array){
	$newArray = Array();
	foreach ($array as $f){
		$newArray[$f] = utf8_encode($f);
	}
	return $newArray;
}

function getInvitation($id){
    if($id){
        $return = Invitation::sql("select * from :table where idInvitation=".$id, SimpleOrm::FETCH_ONE);
        return $return;
    }else{
        return null;
    }
}

function decodeArray($array){
	$newArray = Array();
	foreach ($array as $f){
		$newArray[$f] = utf8_decode($f);
	}
	return $newArray;
}

function debug($var){
	echo "<pre>";
	var_dump($var);
	echo "</pre>";
	die;
}

function senhaBlowFish($newPassword){
	$primeiraParte = substr($newPassword, 0, 7);
	$segundaParte = substr($newPassword,8,strlen($newPassword)-1);
	$primeiraParte = crypt($primeiraParte, BLOWFISH . CUSTO . SALT);
	$segundaParte = crypt($segundaParte, BLOWFISH . CUSTO . SALT);
	$senhaConcat = $primeiraParte . $segundaParte;
	return $senhaConcat;
}

function formatDate($date) {
	$timestamp = strtotime($date);
	return date('d/m/Y', $timestamp);
}

function formatDatePattern($date,$pattern){
	$timestamp = strtotime($date);
	return date($pattern, $timestamp);
}

function date_difference($date1, $date2) {
	return (strtotime($date1) - strtotime($date2))/(3600*24);
}

function get_class_file($className)
{
	$className = strtolower($className);
	$ret = false;
	if (isset($GLOBALS['loadedClasses'][$className]))
	{
		$ret = $GLOBALS['loadedClasses'][$className];
	}
	return $ret;
}

function get_all_classes($subdir = '')
{
	$libpath = $GLOBALS['workingDir'].$GLOBALS['libDir'].DIRECTORY_SEPARATOR;
	//	$classes = getClasses($libpath);
	/*$classes = $GLOBALS['classes'];
	 if (count($classes) == 0) {
		$classes = readCache($GLOBALS['cacheDir']);
		}
		if (is_null($classes))*/
	{
		$classes = getClasses($libpath . $subdir, array());
	}
	$_classes = array();
	foreach($classes as $className => $path)
	{
		$path = substr($path, strlen($libpath));
		//var_dump($path, $subdir, substr_count($path, $subdir));
		if ($subdir == '' || substr_count(strtolower($path), strtolower($subdir)) > 0)
		{
			$_classes[$className] = $path;
		}
	}
	return array_keys($_classes);
}

function postedVars()
{
	return array_keys($_POST);
}

function gettedVars()
{
	return array_keys($_GET);
}

/**
 * Function to access data submitted to the script via POST method.
 *
 *
 * @param $name			the name of the parameter
 * @param $pattern		a regular expression that defines the pattern of the content (it can be an array)
 * @return Mixed
 */
function posted($name, $pattern = PTRN_ALL)
{
	$ret = false;
	if (idx($_POST, $name) !== false)
	{
		if (is_array(el($_POST, $name)))
		{
			$test = el($_POST, $name);
			if (!function_exists('testArray'))
			{
				function testArray($pattern, $array)
				{
					if (!is_array($pattern))
					{
						$pattern = array(PTRN_ALL => $pattern);
					}
						
					$keysToRemove = array_keys($pattern);
						
					$keyPtrn = array_pop($keysToRemove);
					$keysArr = array_keys($array);
					$keysOk = array_reduce($keysArr, create_function('$old, $v', 'return $old && preg_match("' . $keyPtrn . '", $v) == 1;'), true);

					$valPtrn = array_pop($pattern);
					$vals = array_values($array);
					if (isset($vals[0])
							&&	is_array($vals[0]))
					{
						//$valsOk = array_reduce($vals, create_function('$old, $v', 'return $old && testArray(' . escapeshellarg(serialize($valPtrn)) . ', $v) == 1;'), true);
						$valsOk = true;
						foreach($vals as $v)
						{
							$valsOk = $valsOk && testArray($valPtrn, $v);
						}
					} else
					{
						$valsOk = array_reduce($vals, create_function('$old, $v', 'return $old && preg_match("' . $valPtrn . '", $v) == 1;'), true);
					}

					return $valsOk && $keysOk;
				}
			}
			//$f = create_function('$ptrn, $val');
			/*$keyPtrn = array_pop(array_keys($pattern));
			 $keysArr = array_keys($test);
			 $keysOk = array_reduce($keysArr, create_function('$old, $v', 'return $old && preg_match("' . $keyPtrn . '", $v) == 1;'), true);

			 $valPtrn = array_pop($pattern);
			 $vals = array_values($test);
			 $valsOk = array_reduce($vals, create_function('$old, $v', 'return $old && preg_match("' . $valPtrn . '", $v) == 1;'), true);
			 if ($keysOk && $valsOk)*/
			if (testArray($pattern, $test))
			{
				$ret = $test;
			}
		} else if (preg_match($pattern, el($_POST, $name)))
		{
			//$ret = $_POST[$name];
			$ret = utf8_decode(el($_POST, $name));
		}
	}

	return $ret;
}

function getted($name, $pattern = PTRN_ALL)
{
	$ret = false;
	if (idx($_GET, $name) !== false
			&& preg_match($pattern, el($_GET, $name)))
	{
		$ret = utf8_decode(el($_GET, $name));
	}

	return $ret;
}

function requested($string, $pattern = PTRN_ALL){
	$ret = false;
	if (idx($_POST, $string) !== false)
	{
		$ret = posted($string, $pattern);
	} else if (idx($_REQUEST, $string)
			&& preg_match($pattern, el($_REQUEST, $string)))
	{
		$ret = utf8_decode(el($_REQUEST, $string));
	}
	return $ret;
}


function required($name, $pattern = PTRN_ALL)
{
	$ret = false;
	if (isset($GLOBALS['_args'][$name])
			&& preg_match($pattern, $GLOBALS['_args'][$name]))
	{
		$ret = utf8_decode($GLOBALS['_args'][$name]);
	}

	return $ret;
}

function setRequired($args)
{
	foreach($args as $arg => $value)
	{
		$GLOBALS['_args'][$arg] = $value;
	}
}


function rutf8(&$var, $compact = false)
{
	if (is_array($var))
	{
		/* @TODO BETA */
		array_map('rutf8', $var, array_fill(0, count($var), $compact));
	} else
	{
		foreach ($var as $key => $ret)
		{
			if (is_string($ret))
			{
				if ($compact === true)
				{
					$ret = preg_replace('/\<\!\-{2}.*?\-{2}\>/s', '', $ret);
					$ret = str_replace("\t", ' ', $ret); // tab chars
					$ret = str_replace("\r", '', $ret); // strip CR
					$ret = str_replace("\n\n", "\n", $ret); // double NL
				}

				$var -> $key = utf8_encode($ret);
				//$var -> $key = htmlspecialchars_decode(htmlentities($ret,ENT_QUOTES),ENT_QUOTES);
			} else
			{
				@rutf8($var -> $key, $compact);
			}
		}
	}
}

function toJson($var, $compact = false)
{
	$json = $var;
	rutf8($json, $compact);
	return json_encode($json);
}


function el($arr, $index)
{
	$ret = false;
	if (is_array($arr))
	{
		if (count($arr) > 0)
		{
			$map = array_combine(array_map('strtolower', array_keys($arr)), array_keys($arr));
			if (isset($map[strtolower($index)]))
			{
				$ret = &$arr[$map[strtolower($index)]];
			}
			$args = func_get_args();
			array_shift($args);
			array_shift($args);
			while(count($args) > 0 && $ret !== false)
			{
				$nextIndex = array_shift($args);
				if (!is_array($ret))
				{
					$ret = false;
				} else
				{
					$ret = el($ret, $nextIndex);
				}
			}
		}
	} else
	{
		throw new Exception('Not an array');
	}
	return $ret;
}

function objToArray($obj)
{
	$ret = array();
	if (is_object($obj))
	{
		foreach($obj as $prop => $val)
		{
			$ret[$prop] = $val;
		}
	} else
	{
		throw new Exception('Not an object');
	}

	return $ret;
}

function idx($arr, $index)
{
	$ret = false;
	if (is_array($arr))
	{
		@$map = array_combine(array_map('strtolower', array_keys($arr)), array_keys($arr));
		if (isset($map[strtolower($index)]))
		{
			$ret = $map[strtolower($index)];
		}
		$args = func_get_args();
		array_shift($args);
		array_shift($args);
		$sep = md5(microtime(true));
		if (count($args) > 0)
		{
			while(count($args) > 0 && $ret !== false)
			{
				$nextIndex = array_shift($args);
				$hasIndexes = true;
				$tmp = $arr;
				foreach(explode($sep, $ret) as $idx)
				{
					$hasIndexes &= is_array($tmp[$idx]);
					$tmp = $tmp[$idx];
				}
				if (!$hasIndexes)
				{
					$ret = false;
				} else
				{
					$_ret = idx($tmp, $nextIndex);
					$ret = ($_ret === false ? false : $ret . $sep . $_ret);
				}
			}
			if ($ret !== false)
			{
				$ret = explode($sep, $ret);
			}
		}
	} else
	{
		throw new Exception('Not an array');
	}
	return $ret;
}

function randomPassword() {
	$alphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';
	$pass = array(); //remember to declare $pass as an array
	$alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	for ($i = 0; $i < 8; $i++) { //8 characters password
		$n = rand(0, $alphaLength);
		$pass[] = $alphabet[$n];
	}
	return implode($pass); //turn the array into a string
}

/**
 * function returns the pagination of the amount of pages
 *
 * @param int $qtd
 * @return string paginador montado
 */
function getPaginador($qtd, $currentIndex, $functionType) {
	$paginador = "";
	//pega a quantidade de paginas
	$totalPages = intval($qtd/QTD_MESSAGES_PORTAL);
	//se nao completar uma pagina completa de mensagens, adiciona uma pagina para exibir as restantes
	if($qtd % QTD_MESSAGES_PORTAL != 0) {
		$totalPages +=1;
	}

	if ($totalPages > 9) {
		$inicial;
		$final;
		$resto = 0;
		//se o atual pagina esta longe do inicio cria reticencias e indice inicial
		if($currentIndex - 4 > 1) {
			$paginador .= '<li><a id="page_1" href="javascript:'.$functionType.'.changePage(1);">1</a></li>';
			$paginador .= "<b>	...	</b>";
			$inicial = $currentIndex - 4;
			//se nao, verifica o quao perto esta do comeco e adiciona o resto para mostrar indices maiores
		} else {
			$resto = 5 - $currentIndex;
			$inicial = 1;
			$final = $currentIndex + 4 + $resto;
		}
		//se o atual indice esta distante do fim, adiciona 4 indice mais o resto do inicial
		if($currentIndex + 4 <= $totalPages) {
			$final = $currentIndex + 4 + $resto;

			//se nao, pega a diferenca entre o atual indice e o final e diminui o indice inicial para mostrar mais paginas
		} else {
			$resto = 4 - ($totalPages - $currentIndex);
			$inicial -= $resto;
			$final = $totalPages;
		}
		//monta o paginador no intervalo dos dados recolhidos acima
		for ($i = $inicial; $i <= $final; $i++) {
			if($i == $currentIndex) {
				$paginador .= '<li class="active"><a id="page_'.$i.'" href="javascript:'.$functionType.'.changePage('.$i.');">'.$i.'</a></li>';
			} else {
				$paginador .= '<li><a id="page_'.$i.'" href="javascript:'.$functionType.'.changePage('.$i.');">'.$i.'</a></li>';
			}
		}
		//adiciona reticencias e ultima pagina caso o atual indice esteja distante do ultimo
		if($currentIndex + 4 < $totalPages) {
			$paginador .= '<b>	...	</b><li><a id="page_'.$totalPages.'" href="javascript:'.$functionType.'.changePage('.$totalPages.');">'.$totalPages.'</a></li>';;
		}
	} else {
		//se nao tiver mais de 9 paginas, monta paginador normalmente
		for ($i = 1; $i <= $totalPages; $i++) {
			if($i == $currentIndex) {
				$paginador .= '<li class="active"><a id="page_'.$i.'" href="javascript:'.$functionType.'.changePage('.$i.');">'.$i.'</a></li>';
			} else {
				$paginador .= '<li><a id="page_'.$i.'" href="javascript:'.$functionType.'.changePage('.$i.');">'.$i.'</a></li>';
			}
		}
	}
	return $paginador;
}

#=================================================================#
#IMAGE COMPRESSION
#=================================================================#

function compressImage($image64,$int = 0){
	try {

		if($image64 != NULL && $image64 != '' && $image64 != ' '){

			//return $image64;
			$imagePath = SYSTEMDIR . uniqid('image'. time()/* . '_' . $int*/ ).'.jpg';
			//decodifica a imagem e salva no caminho passado
			base64_to_jpeg($image64, $imagePath);

			//compressão da imagem
			$image = base64_decode($image64);
			$img = imagecreatefromstring($image);
			$img = resizeAndCompressImagefunction($img, $imagePath);
			ob_start();
			imagejpeg($img, null, COMPRESSION_PERCENTUAL);
			$data = ob_get_clean();

			$data = imagejpeg(resizeAndCompressImagefunction(imagecreatefromstring($data), $imagePath),$imagePath,COMPRESSION_PERCENTUAL);

			$newImage = file_get_contents($imagePath);
			$newImage64 = base64_encode($newImage);
				
			unlink($imagePath);
				
			return $newImage64;

		}else{
			return '';
		}
			
	} catch ( Exception $e ) {
		echo $e->getMessage ();
		return "";
	}
}

function base64_to_jpeg($base64_string, $output_file) {
	// open the output file for writing
	$ifp = fopen( $output_file, 'wb' );
	fwrite( $ifp, base64_decode( $base64_string ) );
	// clean up the file resource
	fclose( $ifp );
	return $output_file;
}

function resizeAndCompressImagefunction($file, $imagePath, $crop=FALSE) {
	list($width, $height) = getimagesize($imagePath);
	$newwidth = IMAGE_DIMENSION;
	$newheight = IMAGE_DIMENSION;
	$src = imagecreatefromjpeg($imagePath);
	$dst = imagecreatetruecolor($newwidth, $newheight);
	imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
	return $dst;
}

#=================================================================#
#=================================================================#
?>